<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="root.persistence.entities.MasterOxford"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<MasterOxford> dicc = (List<MasterOxford>) request.getAttribute("lista");
    Iterator<MasterOxford> iterDicc = dicc.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Evaluación final</h1>

        <h1>Diccionario Oxford</h1>
        <h2>Alumno: Alvaro Torres Silva</h2>
        <label>Palabra Consultada :</label><br>

        <input name="palabra" value="<%= request.getAttribute("palabra")%>" disabled class="form-control">
        <br>
        <label for="nombre">Significado :</label>
        <br>
        <textarea name="significado" rows="2" cols="50" disabled class="form-control">
            <%= request.getAttribute("definicion").toString().trim()%>
        </textarea>
        <br>
        <input type="button" value="Volver"/>
        <h2>Lista de Consultas</h2>
        <br>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Palabra</th>    
                    <th>Significado</th>
                </tr>
            </thead>
            <tbody>                    
                <%
                    while (iterDicc.hasNext()) {
                        MasterOxford diccionario = iterDicc.next();

                %>
                <tr>
                    <td><%= diccionario.getPalabra()%></td>
                    <td><%= diccionario.getDefinicion()%></td>
                </tr>
                <%
                    }
                %>                    
            </tbody>
        </table>
    </body>
</html>
