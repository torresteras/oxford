/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dto.Palabra;

import java.util.List;

/**
 *
 * @author alvaro
 */
public class Resultado {
 private Long id;
    private String palabra;
    private List<String> definiciones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public List<String> getDefiniciones() {
        return definiciones;
    }

    public void setDefiniciones(List<String> definiciones) {
        this.definiciones = definiciones;
    }

    

}