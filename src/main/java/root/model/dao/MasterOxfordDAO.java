/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.persistence.entities.MasterOxford;

/**
 *
 * @author logistica
 */
public class MasterOxfordDAO implements Serializable {

    public MasterOxfordDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public MasterOxfordDAO() {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MasterOxford masterOxford) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(masterOxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMasterOxford(masterOxford.getId()) != null) {
                throw new PreexistingEntityException("MasterOxford " + masterOxford + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MasterOxford masterOxford) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            masterOxford = em.merge(masterOxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = masterOxford.getId();
                if (findMasterOxford(id) == null) {
                    throw new NonexistentEntityException("The masterOxford with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MasterOxford masterOxford;
            try {
                masterOxford = em.getReference(MasterOxford.class, id);
                masterOxford.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The masterOxford with id " + id + " no longer exists.", enfe);
            }
            em.remove(masterOxford);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MasterOxford> findMasterOxfordEntities() {
        return findMasterOxfordEntities(true, -1, -1);
    }

    public List<MasterOxford> findMasterOxfordEntities(int maxResults, int firstResult) {
        return findMasterOxfordEntities(false, maxResults, firstResult);
    }

    private List<MasterOxford> findMasterOxfordEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MasterOxford.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MasterOxford findMasterOxford(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MasterOxford.class, id);
        } finally {
            em.close();
        }
    }

    public int getMasterOxfordCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MasterOxford> rt = cq.from(MasterOxford.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//
//    public List<MasterOxford> MasterOxford(String palabra) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public void create(Palabra palabraBD) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    
}
