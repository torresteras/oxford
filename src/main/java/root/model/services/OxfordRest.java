/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static jdk.nashorn.internal.runtime.Debug.id;
import root.model.dao.MasterOxfordDAO;
import root.persistence.entities.MasterOxford;

/**
 *
 * @author logistica
 */
@Path("diccionario")
public class OxfordRest {


    MasterOxfordDAO dao = new MasterOxfordDAO();
    private final Logger log = Logger.getLogger(this.getClass().getName());
    @GET
//    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public String listarTodo(@HeaderParam("api-key") String apikey, @HeaderParam("api-id") String apiId, @PathParam("palabra") String palabra) {
                log.info("Entra aca?");

        List<MasterOxford> lista = dao.findMasterOxfordEntities();
        System.out.println("REST diccionario:  apikey" + apikey);
        System.out.println("REST diccionario:  apiId" + apiId);
        System.out.println("REST diccionario :    palabra  " + palabra);
        System.out.println("Obteniendo datos del dao");

        final String language = "es";
        final String word = palabra;
        final String fields = "definitions";
        final String strictMatch = "true";
        final String word_id = word.toLowerCase();
        final String urlOxford = "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word_id + "?" + "fields=" + fields + "&strictMatch=" + strictMatch;

        try {
            URL url = new URL(urlOxford);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json charset=UTF-8");
            urlConnection.setRequestProperty("app_id", apiId);
            urlConnection.setRequestProperty("app_key", apikey);

//            Ejecucion de API Oxford 
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            System.out.println("Respuesta Oxford : " + reader);
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            System.out.println("Respuesta 2 Oxford : " + stringBuilder.toString());
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        MasterOxford palabra = dao.findMasterOxford(idbuscar);
        return Response.ok(200).entity(palabra).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(MasterOxford palabraNueva) throws Exception {

        MasterOxfordDAO palabra = new MasterOxfordDAO();
        palabra.create(palabraNueva);
        return Response.ok("Palabra Creada").build();
    }
}
