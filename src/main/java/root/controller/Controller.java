package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import root.dto.Palabra.Resultado;
import root.persistence.entities.MasterOxford;
import root.model.dao.MasterOxfordDAO;

@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final String json;

        MasterOxfordDAO dao = new MasterOxfordDAO();

        String palabra = request.getParameter("palabra");

        log.info("Inicio Controller ");
        log.log(Level.INFO, "Busqueda palabra: {0}", palabra);

        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://oxfordapp.herokuapp.com/api/diccionario/" + palabra);

        log.info("Pasó el link y inicia json ");

        json = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "4bae6000b0ac9131a440b5ffe064c099").header("api-id", "33ab078e").get(String.class);

        log.info("Inicio JSON Parser ");

        JSONParser parser = new JSONParser();
        String ret = "404";
        Resultado res = null;
        try {

            final String result = json;
            final Object parse = parser.parse(result);
            ret = (String) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) parse)
                    .get("results")).get(0)).get("lexicalEntries")).get(0)).get("entries"))
                    .get(0)).get("senses")).get(0)).get("definitions")).get(0);

            log.info("Muere la app aca?");

        } catch (ParseException e) {

            System.out.println(e);
        }

        MasterOxford dic = new MasterOxford();
        dic.setPalabra(palabra);
        if (!"404".equals(ret)) {

            dic.setId(ret);
            dic.setDefinicion(ret);

        } else {
//            dic.setId("1");
            dic.setDefinicion("-- Sin Resultado --");
            ret = dic.getDefinicion();
        }
        try {
            dao.create(dic);


        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.info("que muestra??");

   
        List<MasterOxford> dict = dao.findMasterOxfordEntities();

        request.setAttribute("palabra", palabra);
        request.setAttribute("definicion", ret);
        request.setAttribute("lista", dict);
        request.getRequestDispatcher("lista.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}