/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author logistica
 */
@Entity
@Table(name = "master_oxford")
@NamedQueries({
    @NamedQuery(name = "MasterOxford.findAll", query = "SELECT m FROM MasterOxford m"),
    @NamedQuery(name = "MasterOxford.findById", query = "SELECT m FROM MasterOxford m WHERE m.id = :id"),
    @NamedQuery(name = "MasterOxford.findByPalabra", query = "SELECT m FROM MasterOxford m WHERE m.palabra = :palabra"),
    @NamedQuery(name = "MasterOxford.findByDefinicion", query = "SELECT m FROM MasterOxford m WHERE m.definicion = :definicion")})
public class MasterOxford implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
//    @Basic(optional = false)
//    @NotNull
    @Size(max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "definicion")
    private String definicion;

    public MasterOxford() {
    }

    public MasterOxford(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MasterOxford)) {
            return false;
        }
        MasterOxford other = (MasterOxford) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entities.MasterOxford[ id=" + id + " ]";
    }
}
